#!/bin/bash
echo "Cloning https://github.com/artisynth/artisynth_core"
git clone https://github.com/artisynth/artisynth_core
cd artisynth_core


echo "Updating artisynth libraries"
bin/updateArtisynthLibs

echo "Building ArtiSynth"
make

echo "Build Complete! Trying to launch artisynth"
bin/artisynth

