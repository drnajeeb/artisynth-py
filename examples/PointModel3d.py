from artisynth.PyArtiSynth import PyArtiSynth, logger
from jnius import cast
import numpy as np

class PointModel3d(PyArtiSynth):
    def __init__(self, modelpath, mech_model, **kwargs) -> None:
        super(PointModel3d, self).__init__(modelpath, mech_model, **kwargs)
        self.p2p_muscles = self.get_point_to_point_muscles()
        self.frame_markers = self.get_frame_markers()

        # remove any existing input probes and controllers
        self.artisynth.getRootModel().removeAllInputProbes()
        self.artisynth.getRootModel().removeAllControllers()

    def step(self, excitation):
        for i, e in enumerate(excitation):
            self.p2p_muscles[i].setExcitation(float(e))
        self.step_and_wait()
        return self.get_state()

    def reset(self):
        self.artisynth.reset()
        return self.get_state()
    
    def get_state(self):
        state = []
        for marker in self.frame_markers:
            p, v = marker.getPosition(), marker.getVelocity()
            state.extend([p.x, p.y, p.z, v.x, v.y, v.z])
        return state

    def get_point_to_point_muscles(self):
        axial_springs = []
        for i in range(self._mech_model.axialSprings().numComponents()):
            axial_springs.append(
                cast("artisynth.core.mechmodels.Muscle",
                     self._mech_model.axialSprings().getByNumber(i)))
            logger.info(
                f'Registering point-to-point muscle: {axial_springs[-1].getName()}')
        return axial_springs

    def get_frame_markers(self):
        frame_markers = []
        for i in range(self._mech_model.frameMarkers().numComponents()):
            frame_markers.append(
                cast('artisynth.core.mechmodels.FrameMarker', 
                self._mech_model.frameMarkers().getByNumber(i)))
            logger.info(
                f'Registering frame marker: {frame_markers[-1].getName()}')
        return frame_markers


if __name__ == "__main__":
    point_model = PointModel3d(
        modelpath="artisynth.demos.inverse.PointModel3d", 
        mech_model='models/point')
   
    # remove any existing input probes and controllers
    point_model.artisynth.getRootModel().removeAllInputProbes()
    point_model.artisynth.getRootModel().removeAllControllers() 

    # and then apply random activation to 8 muscles
    for i in range(1000):
        activation = np.random.rand(8)
        state = point_model.step(activation)
        print(state)