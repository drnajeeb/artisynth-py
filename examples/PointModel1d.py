from artisynth.PyArtiSynth import PyArtiSynth, logger
from jnius import cast
import numpy as np

class PointModel1d(PyArtiSynth):
    def __init__(self, modelpath, mech_model, **kwargs) -> None:
        super(PointModel1d, self).__init__(modelpath, mech_model, **kwargs)
        self.p2p_muscles = self.get_point_to_point_muscles()
        self.controlled_particle = self.get_particles()[1]

    def step(self, excitation):
        for i, e in enumerate(excitation):
            self.p2p_muscles[i].setExcitation(float(e))
        self.step_and_wait()
        return self.get_state()

    def reset(self):
        self.artisynth.reset()
        return self.get_state()
    
    def get_state(self):
        p, v = self.controlled_particle.getPosition(), self.controlled_particle.getVelocity()
        return [p.x, v.x]

    def get_point_to_point_muscles(self):
        axial_springs = []
        for i in range(self._mech_model.axialSprings().numComponents()):
            axial_springs.append(
                cast("artisynth.core.mechmodels.Muscle",
                     self._mech_model.axialSprings().getByNumber(i)))
            logger.info(
                f'Registering point-to-point muscle: {axial_springs[-1].getName()}')
        return axial_springs

    def get_particles(self):
        particles = []
        for i in range(self._mech_model.particles().numComponents()):
            particles.append(
                cast('artisynth.core.mechmodels.Particle', 
                self._mech_model.particles().getByNumber(i)))
            logger.info(
                f'Registering frame marker: {particles[-1].getName()}')
        return particles


if __name__ == "__main__":
    point_model = PointModel1d(
        modelpath="artisynth.demos.inverse.PointModel1d", 
        mech_model='models/point', gui=True)
   
    # remove any existing input probes and controllers
    point_model.artisynth.getRootModel().removeAllInputProbes()
    point_model.artisynth.getRootModel().removeAllControllers()

    # and then apply random activation to 2 muscles
    for i in range(1000):
        activation = np.random.rand(2)
        state = point_model.step(activation)
        print(state)