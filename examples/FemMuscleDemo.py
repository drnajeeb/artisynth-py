from artisynth.PyArtiSynth import PyArtiSynth, logger
from jnius import cast
import numpy as np

class FemMuscleDemo(PyArtiSynth):
    def __init__(self, modelpath, mech_model, **kwargs) -> None:
        super(FemMuscleDemo, self).__init__(modelpath, mech_model, **kwargs)
        self.fem_muscle_model = cast(
            "artisynth.core.femmodels.FemMuscleModel",
            self._mech_model.findComponent('models/fem'))
        self.nodes = self.get_nodes()
        self.muscle_bundles = self.get_muscle_bundles()

        # remove any existing input probes and controllers
        self.artisynth.getRootModel().removeAllInputProbes()
        self.artisynth.getRootModel().removeAllControllers()

    def step(self, excitation):
        for i, e in enumerate(excitation):
            self.muscle_bundles[i].setExcitation(float(e))
        self.step_and_wait()
        return self.get_state()

    def reset(self):
        self.artisynth.reset()
        return self.get_state()
    
    def get_state(self):
        state = []
        for node in self.nodes:
            if not node.isDynamic():
                continue
            p, v = node.getPosition(), node.getVelocity()
            state.extend([p.x, p.y, p.z, v.x, v.y, v.z])
        return state

    def get_nodes(self):
        nodes = []
        for i in range(self.fem_muscle_model.getNodes().numComponents()):
            nodes.append(cast(
                'artisynth.core.femmodels.FemNode3d',
                self.fem_muscle_model.getNodes().getByNumber(i)))
        return nodes

    def get_muscle_bundles(self):
        muscle_bundles = []
        for i in range(self.fem_muscle_model.getMuscleBundles().numComponents()):
            muscle_bundles.append(
                cast("artisynth.core.femmodels.MuscleBundle",
                     self.fem_muscle_model.getMuscleBundles().getByNumber(i)))
            logger.info(
                f'Registering muscle bundle: {muscle_bundles[-1].getName()}')
        return muscle_bundles


if __name__ == "__main__":
    fem_model = FemMuscleDemo(
        modelpath="artisynth.demos.fem.FemMuscleDemo", 
        mech_model='models/FemBeam',
        gui=True)
   
    # and then apply random activation to the 2 muscle bundles
    for i in range(1000):
        activation = np.random.rand(2)/200
        state = fem_model.step(activation)
        print(state[:6])
