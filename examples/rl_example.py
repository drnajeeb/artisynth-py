import gym
import numpy as np
from scipy import linalg as la
import ray
from ray.rllib.agents import ppo, ddpg
ray.init()


class PointModel3dEnv(gym.Env):
  def __init__(self, env_config=None):
    super(PointModel3dEnv, self).__init__()
    from examples.PointModel3d import PointModel3d

    self.sim = PointModel3d(modelpath="artisynth.demos.inverse.PointModel3d", mech_model='models/point', gui=False)
    self.sim.artisynth.getRootModel().removeAllInputProbes()
    self.sim.artisynth.getRootModel().removeAllControllers()
    
    num_of_activations = len(self.sim.p2p_muscles)
    self.action_space = gym.spaces.Box(low=0.05, high=0.95, shape=(num_of_activations,), dtype=np.float32)
    
    self.observation_space = gym.spaces.Box(low=float('-inf'), high=float('inf'), shape=(6,), dtype=np.float32)

    self.previousError = float('inf')
    
  def step(self, action):
      full_state = self.sim.step(action)
      reward, is_done = self.getReward(action, full_state)
      return full_state, reward, is_done, {}

  def getReward(self, action, observ):
    target = [0.5,0.8,0.8,0,0,0]
    dthreshold = 0.2
    error = la.norm(np.array(target[:3]) - np.array(observ[:3]))
    t = self.sim.artisynth.getTime()
    reward = -la.norm(action)
    if error < self.previousError:
        reward = 1./t
        self.previousError = error
    if error < dthreshold:
        reward = 100.
    return reward, error < dthreshold

  def reset(self):
      return self.sim.reset()


def train_policy():
    ppo_config = {
        "num_workers": 10,
        "batch_mode": "complete_episodes",
        "rollout_fragment_length":100, 
        "vf_clip_param":5000, 
        "sgd_minibatch_size":100,
        "train_batch_size":100}

    agent = ppo.PPOTrainer(
        env=PointModel3dEnv,
        config=ppo_config)

    for i in range(1001):
        result = agent.train()
        # print(result)
        if i%100==0:
            agent.save('pos_threshold_0_2')

if __name__ == '__main__':
    train_policy()

