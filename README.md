# Python Bindings for ArtiSynth
An ultra-thin Python wrapper for ArtiSynth.

## Installation
Java JDK 1.8 is a requirement.  Once you have JDK installed clone this repository.

### Step 1. Install ArtiSynth
Install ArtiSynth if you have not installed it already as follows
```
> . ./artisyth_install.sh
```
### Step 2. Install Python requirements
The next step is to install the Python requirements
```
pip install -r requirements.txt
```
### Step 3. Configure the environment
Finally configure the `JAVA_PATH` and `ARTISYNTH_HOME` variables in `artisynth/configuration.py:10,14`.

Set the `-noGui` option if GUI is not desired.

## How to use
In the repo directory 
```
> export PYTHONPATH=.
```

```
python examples/PointModel3d.py
```
## Examples:
**Launcher**: Demonstrates how to launch an ArtiSynth instance from Python

**MuscleArm**: Example of rigid body and point to point muscle model

**PointModel3d**: Example of a 3D point to point muscle model

**FemMuscleDemo**: Example of controlling an FEM model with muscle bundles.

**RL Example**: Coming soon (skipped here to keep the dependencies to a minimum, i.e., artisynth-py does not need to depend on gym or rllib.)

