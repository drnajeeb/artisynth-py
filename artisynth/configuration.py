from collections import namedtuple


ConfigType = namedtuple(
  'ConfigType', 
  ['ARTISYNTH_HOME', 'ARTISYNTH_ARGS', 'JAVA_HOME', 'JVM_OPTIONS' ])

config = ConfigType(
  # ArtiSynth
  ARTISYNTH_HOME='/content/artisynth_core',
  ARTISYNTH_ARGS=['-numSolverThreads', '2'],

  # JVM
  JAVA_HOME='/usr/lib/jvm/java-8-openjdk-amd64/',
  JVM_OPTIONS=['-Xms200M', '-Xmn100M', '-Xmx6G'])
