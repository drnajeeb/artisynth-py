import logging
from .configuration import config
import jnius_config
import os
from time import sleep


os.environ['JAVA_HOME'] = config.JAVA_HOME
jnius_config.add_options(*config.JVM_OPTIONS)
jnius_config.set_classpath(
    os.path.join(config.ARTISYNTH_HOME, "classes"),
    os.path.join(config.ARTISYNTH_HOME, "lib/*"))
logging.basicConfig(filename='log.txt',
                    filemode='w',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger()

from jnius import autoclass
from jnius import cast


class PyArtiSynth:
    def __init__(self, modelpath=None, mech_model=None, solver_nthreads=2, gui=False):
        super(PyArtiSynth, self).__init__()
        Launcher = autoclass('artisynth.core.driver.Launcher')
        artisynth_cmd_args = ['-numSolverThreads', str(solver_nthreads)]
        if not gui: artisynth_cmd_args.append('-noGui')

        self.artisynth = Launcher.dolaunch(artisynth_cmd_args)
        if modelpath and mech_model:
            self.artisynth.loadModel(modelpath, modelpath.split('.')[-1], [])
            mech_model = self.artisynth.getRootModel().findComponent(mech_model)
            self._mech_model = cast("artisynth.core.mechmodels.MechModel", mech_model)

            

    def step_and_wait(self):
        t0 = self.artisynth.getTime()
        self.artisynth.step()
        while self.artisynth.getTime() <= t0:
            sleep(self._mech_model.getMaxStepSize())
